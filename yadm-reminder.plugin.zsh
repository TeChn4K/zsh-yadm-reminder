_prompt_yadm_reminder () {
	if [[ $ZSH_PLUGINS_YADM_REMINDER_ENABLE != false ]]; then
		local color=${ZSH_PLUGINS_YADM_REMINDER_COLOR-196}
		if ! command -v yadm >/dev/null 2>&1; then
			print -P "%B%F{$color}Yadm is not installed... \nInstall Yadm or disable this plugin with ZSH_PLUGINS_YADM_REMINDER_ENABLE=false%f%b"
		elif [[ `yadm status -uno --porcelain ~` ]]; then
		    local text=${ZSH_PLUGINS_YADM_REMINDER_TEXT-There are changes in your dotfiles! Yadm commit required.}
			print -P "%B%F{$color}$text%f%b"
		fi
	fi
}

autoload -Uz add-zsh-hook
add-zsh-hook precmd _prompt_yadm_reminder