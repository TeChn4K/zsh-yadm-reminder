# ZSH Yadm reminder

A Zsh plugin to remind you when your dotfiles contain uncommited changes with **yadm**

## Customizations

You can customize this plugin with some environment variables, e.g. in your `.zshrc`

### Text value

You can customize the text with `ZSH_PLUGINS_YADM_REMINDER_TEXT`

```
ZSH_PLUGINS_YADM_REMINDER_TEXT="Do not forget to yadm commit!"

```

### Text color

You can customize the text color with `ZSH_PLUGINS_YADM_REMINDER_COLOR`

Must be a number, one of the 256 available in your 256 Xterm colors!

Pick one here : https://jonasjacek.github.io/colors/

```
ZSH_PLUGINS_YADM_REMINDER_COLOR=190

```

### Disable plugin

You can disable this plugin with `ZSH_PLUGINS_YADM_REMINDER_ENABLE`

```
ZSH_PLUGINS_YADM_REMINDER_ENABLE=false

```